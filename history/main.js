// Validación del formulario
const form = document.getElementById('paciente-form');
form.addEventListener('submit', (event) => {
    event.preventDefault();

    const nombre = document.getElementById('nombre');
    const apellido = document.getElementById('apellido');
    const cedula = document.getElementById('cedula');
    const edad = document.getElementById('edad');
    const sexo = document.getElementById('sexo');
    const direccion = document.getElementById('direccion');
    const fecha = document.getElementById('fecha');
    const sintomas = document.getElementById('sintomas');
    const diagnostico = document.getElementById('diagnostico');
    const tratamiento = document.getElementById('tratamiento');
    const observaciones = document.getElementById('observaciones');

    // Validación de campo nombre
    if (nombre.value === '') {
        document.getElementById('nombre-error').textContent = 'Este campo es obligatorio';
        nombre.focus();
        return;
    }
    document.getElementById('nombre-error').textContent = '';

    // Validación de campo apellido
    if (apellido.value === '') {
        document.getElementById('apellido-error').textContent = 'Este campo es obligatorio';
        apellido.focus();
        return;
    }
    document.getElementById('apellido-error').textContent = '';

    // Validación de campo cedula
    if (cedula.value === '') {
        document.getElementById('cedula-error').textContent = 'Este campo es obligatorio';
        cedula.focus();
        return;
    }
    document.getElementById('cedula-error').textContent = '';

    // Validación de campo edad
    if (edad.value === '') {
        document.getElementById('edad-error').textContent = 'Este campo es obligatorio';
        edad.focus();
        return;
    }
    document.getElementById('edad-error').textContent = '';

    // Validación de campo sexo
    if (sexo.value === '') {
        document.getElementById('sexo-error').textContent = 'Este campo es obligatorio';
        sexo.focus();
        return;
    }
    document.getElementById('sexo-error').textContent = '';

    // Validación de campo direccion
    if (direccion.value === '') {
        document.getElementById('direccion-error').textContent = 'Este campo es obligatorio';
        direccion.focus();
        return;
    }
    document.getElementById('direccion-error').textContent='';
        
    // Validación de campo fecha
    if (fecha.value === '') {
        document.getElementById('fecha-error').textContent = 'Este campo es obligatorio';
        fecha.focus();
        return;
    }
    document.getElementById('fecha-error').textContent = '';

    // Validación de campo sintoma
    if (sintomas.value === '') {
        document.getElementById('sintomas-error').textContent = 'Este campo es obligatorio';
        sintomas.focus();
        return;
    }
    document.getElementById('sintomas-error').textContent = '';

    // Validación de campo diagnostico
    if (diagnostico.value === '') {
        document.getElementById('diagnostico-error').textContent = 'Este campo es obligatorio';
        diagnostico.focus();
        return;
    }
    document.getElementById('diagnostico-error').textContent = '';

    // Validación de campo tratamiento
    if (tratamiento.value === '') {
        document.getElementById('tratamiento-error').textContent = 'Este campo es obligatorio';
        tratamiento.focus();
        return;
    }
    document.getElementById('tratamiento-error').textContent = '';

    // Validación de campo observacion
    if (observaciones.value === '') {
        document.getElementById('observaciones-error').textContent = 'Este campo es obligatorio';
        observaciones.focus();
        return;
    }
    document.getElementById('observaciones-error').textContent = '';

    // Envío del formulario
    form.submit();
});

// Acciones de los botones
const imprimirBtn = document.getElementById('imprimir');
const eliminarBtn = document.getElementById('eliminar');
const editarBtn = document.getElementById('editar');

imprimirBtn.addEventListener('click', () => {
    window.print(); // Imprime la página actual
    });

eliminarBtn.addEventListener('click', () => {
    // Código para eliminar
});

editarBtn.addEventListener('click', () => {
    // Código para editar
});
 // estos botonos no estan funciones por que necesitan una base de datos para ser funcionales
 // solo esta funcional el boton imprimir 