function validate() {
  // Obtener los valores de los campos del formulario
  var email = document.getElementById("email").value;
  var newPassword = document.getElementById("new-password").value;
  var confirmPassword = document.getElementById("confirm-password").value;

  // Obtener los elementos span para cada campo
  var emailError = document.getElementById("email-error");
  var passwordError = document.getElementById("password-error");
  var confirmError = document.getElementById("confirm-error");

  // Validar el campo email
  if (!emailIsValid(email)) {
    emailError.style.display = "inline";
    emailError.textContent = "Por favor ingrese un correo electrónico válido.";
    return false;
  } else {
    emailError.style.display = "none";
  }

  // Validar el campo nueva contraseña
  if (!passwordIsValid(newPassword)) {
    passwordError.style.display = "inline";
    passwordError.textContent = "La contraseña debe tener al menos 8 caracteres y contener al menos una letra y un número.";
    return false;
  } else {
    passwordError.style.display = "none";
  }

  // Validar el campo confirmar contraseña
  if (newPassword !== confirmPassword) {
    confirmError.style.display = "inline";
    confirmError.textContent = "Las contraseñas no coinciden. Por favor inténtelo de nuevo.";
    return false;
  } else {
    confirmError.style.display = "none";
  }

  // Si todos los campos son válidos, devolver verdadero
  return true;
}

function emailIsValid(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

function passwordIsValid(password) {
  return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(password);
}