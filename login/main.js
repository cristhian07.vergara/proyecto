// Obtener los elementos del DOM
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("contraseña");
const loginButton = document.querySelector(".btn-block");

// Agregar un controlador de eventos al botón de inicio de sesión
loginButton.addEventListener("click", function(event) {
  // Validar el campo de correo electrónico
  if (!emailInput.checkValidity()) {
    event.preventDefault(); // Prevenir el envío del formulario
    alert("Por favor ingrese un correo electrónico válido");
  }

  // Validar el campo de contraseña
  if (!passwordInput.checkValidity()) {
    event.preventDefault(); // Prevenir el envío del formulario
    alert("Por favor ingrese una contraseña");
  }
});