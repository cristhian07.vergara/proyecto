function mostrarSugerencia(str) {
    // Variable para almacenar el paciente elegido
    var pacienteElegido='';
    // Verificar el valor de 'str' y asignar el paciente correspondiente
    if (str=='paciente1') {pacienteElegido='paciente1';} else if (str=='paciente2') {pacienteElegido='paciente2';} else if (str=='paciente3') {pacienteElegido='paciente3';}
    else if (str=='paciente4') {pacienteElegido='paciente4';}  else if (str=='paciente5') {pacienteElegido='paciente5';}  else if (str=='paciente6') {pacienteElegido='paciente6';} 
    else if (str=='paciente7') {pacienteElegido='paciente7';}  else if (str=='paciente8') {pacienteElegido='paciente8';}  else if (str=='paciente9') {pacienteElegido='paciente9';} 
    else if (str=='paciente10') {pacienteElegido='paciente10';}  else {pacienteElegido='none';}
    var xmlhttp;
    // Verificar si no hay datos o el paciente elegido es 'none'
    if (str.length==0 ||pacienteElegido=='none') { document.getElementById("txtInformacion").innerHTML="no hay datos";
    mostrarpaciente(); return; }
    // Crear una instancia de XMLHttpRequest
    xmlhttp=new XMLHttpRequest();
    // Definir la función de devolución de llamada cuando se realiza una solicitud
    xmlhttp.onreadystatechange = function() {
        // Obtener la respuesta JSON y convertirla en objeto
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
     var jsonResponse = xmlhttp.responseText;
     var objeto_json = JSON.parse(jsonResponse);
      // Obtener la lista de pacientes del objeto JSON
     pacienteRecibidos = objeto_json.listadopaciente.paciente;
     // Recorrer la lista de pacientes
     for (var i=0; i<pacienteRecibidos.length;i++) {
     var nombrepaciente = objeto_json.listadopaciente.paciente[i].nombre;
     if (nombrepaciente==pacienteElegido) {
     document.getElementById("txtInformacion").innerHTML = ' El historial del paciente contiene  los siguientes datos '
    +nombrepaciente;
     // Obtener los datos del paciente seleccionado
     var selectpaciente = objeto_json.listadopaciente.paciente[i].datos;
      // Mostrar los datos del paciente seleccionado
     mostrarpaciente(selectpaciente);
     }
     }
    
    }
    }
     // Realizar una solicitud GET para obtener un archivo JSON
    xmlhttp.open("GET", "json.json?nocache=" + (new Date()).getTime());
    xmlhttp.send();
    }
    // Función para mostrar los pacientes
    function mostrarpaciente (arraypaciente) {
    var nodoMostrarResultados = document.getElementById('listapaciente');
    if (!arraypaciente) {nodoMostrarResultados.innerHTML = ''; return}
    var contenidosAMostrar = '';
    for (var i=0; i<arraypaciente.length;i++) {
     contenidosAMostrar = contenidosAMostrar+'<div id="paciente'+i+'"> ';
     contenidosAMostrar += '<a href="">' + arraypaciente[i]+ '</a></div>';
    }
    if (contenidosAMostrar) {nodoMostrarResultados.innerHTML = contenidosAMostrar;}
    }

    document.querySelectorAll('.printbutton').forEach(function(element) {
        element.addEventListener('click', function() {
            print();
        });
      });